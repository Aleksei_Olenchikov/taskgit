﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace LibiryWeb
{
    //Класс Каталог. Содиржит в себе 3 списка Книги, Газеты, Патенты.
    //А также методы для работы с каталогом.
    public class Katalog
    {
        /*private List<Book> books;
        private List<Paper> papers;
        private List<Patent> patents;*/
        private List<Record> records;

        public List<Record> GetRecords()
        {
            return this.records;
        }

        //Констукторы каталога.
        public Katalog()
        {
            /*books = new List<Book>();
            papers = new List<Paper>();
            patents = new List<Patent>();*/
            records = new List<Record>();
        }
        public Katalog(List<Record> records)
        {
            this.records = records;
        }


        // Методы добавления записей в каталог.
        /*public void AddBook(Book b)
        {
            if (b is null) throw new System.Exception("На вход методу AddBook подан null");
            else records.Add(b);
        }
        public void AddPaper(Paper p)
        {
            if (p is null) throw new System.Exception("На вход методу AddPaper подан null");
            else records.Add(p);
        }
        public void AddPatent(Patent p)
        {
            if (p is null) throw new System.Exception("На вход методу AddPatent подан null");
            else records.Add(p);
        }*/
        public void AddRecord(Record a)
        {
            if(a is null) throw new System.Exception("На вход методу AddRecord подан null");
            this.records.Add(a);
        }

        //Методы удаления записей из каталога.
        /*public void DelBook(int id)
        {
            for (int i = 0; i < books.Count; i++)
                if (books[i].ID == id) books.RemoveAt(i);
        }
        public void DelPaper(int id)
        {
            for (int i = 0; i < papers.Count; i++)
                if (papers[i].ID == id) papers.RemoveAt(i);
        }
        public void DelPatent(int id)
        {
            for (int i = 0; i < patents.Count; i++)
                if (patents[i].registationNumber == id) patents.RemoveAt(i);
        }*/
        public void DelRecord(int id)
        {
            for (int i = 0; i < records.Count; i++)
                if (records[i].ID == id) records.RemoveAt(i);
        }

        //Метод печати каталога в консоль.
        //Сначала если есть книги, то книги, потом газеты, потом патенты.
        public void WriteKatalog()
        {
            /*foreach (Book i in this.books)
                i.Write();
            foreach (Paper i in this.papers)
                i.Write();
            foreach (Patent i in this.patents)
                i.Write();*/
            bool flag = false;
            //Console.WriteLine("Книги : ");
            foreach (Record i in this.records)
                if (i is Book)
                {
                    if (!flag)
                    {
                        flag = true;
                        Console.WriteLine("Книги : ");
                    }
                    i.Write();
                }
            //Console.WriteLine("Газеты : ");
            flag = false;
            foreach (Record i in this.records)
                if (i is Paper)
                {
                    if (!flag)
                    {
                        flag = true;
                        Console.WriteLine("Газеты : ");
                    }
                    i.Write();
                }
            //Console.WriteLine("Патенты : ");
            flag = false;
            foreach (Record i in this.records)
                if (i is Patent)
                {
                    if (!flag)
                    {
                        flag = true;
                        Console.WriteLine("Патенты : ");
                    }
                    i.Write();
                }

        }


        //Метод поиска по названию. Просто проходим по всем типам и ищем нужное имя. 
        //После этого всё что найдём подаём в новый каталог.
        public Katalog FindByName(string name)
        {
            Katalog result = new Katalog();
            /*foreach (Book i in books)
                if (i.name == name) result.AddBook(i);
            foreach (Paper i in papers)
                if (i.name == name) result.AddPaper(i);
            foreach (Patent i in patents)
                if (i.name == name) result.AddPatent(i);*/
            foreach (Record i in records)
                if (i.name == name) result.records.Add(i);
            return result;
        }

        //Метод поика книг по автору.
        public Katalog FindBookByAutor(string autor)
        {
            Katalog result = new Katalog();
            /*foreach (Book i in books)
                foreach (string j in i.autors)
                    if (j == autor)
                    {
                        result.books.Add(i);
                        break;
                    }*/
            foreach (Record i in records)
                if (i is Book)
                    foreach (string j in i.autors)
                        if (j == autor)
                        {
                            result.records.Add(i);
                            break;
                        }
            return result;
        }

        //Метод вывода в консоль всех кних, название издательства которых начинаются с определённой подстроки.
        //А потом сортировка по издательствам.
        public void WriteBookByPublish(string substringPublish)
        {
            Katalog result = new Katalog();
            /*foreach(Book i in books)
                if (i.namePublish.StartsWith(substringPublish)) result.books.Add(i);
            for(int i = 0; i < result.books.Count; i++)
            {
                for(int j = i + 1; j < result.books.Count; j++)
                {
                    if (result.books[i].namePublish.CompareTo(result.books[j].namePublish) == 0)
                    {
                        Book tmp = new Book();
                        tmp = result.books[i];
                        result.books[i] = result.books[j];
                        result.books[j] = tmp;
                    }
                }
            }*/

            foreach (Record i in records)
                if (i.namePublish.StartsWith(substringPublish)) result.records.Add(i);
            for (int i = 0; i < result.records.Count; i++)
            {
                for (int j = i + 1; j < result.records.Count; j++)
                {
                    if (result.records[i].namePublish.CompareTo(result.records[j].namePublish) == 0)
                    {
                        Record tmp;
                        tmp = result.records[i];
                        result.records[i] = result.records[j];
                        result.records[j] = tmp;
                    }
                }


                result.WriteKatalog();
            }
        }

        //Метод сортировки записей по году издания.
        //dec - параметр, по убыванию ли сортировать или нет.
        public void SortRecordsByYear(bool dec)
        {
            /*for(int i = 0; i < records.Count-1; i++)
            {
                for(int j = i+1; j < records.Count; j++)
                {
                    if (!dec)
                        if (records[i].Year() > records[j].Year())
                        {
                            Record tmp;
                            tmp = records[i];
                            records[i] = records[j];
                            records[j] = tmp;
                        }
                    else
                        if (records[i].Year() < records[j].Year())
                        {
                            Record tmp;
                            tmp = records[i];
                            records[i] = records[j];
                            records[j] = tmp;
                        }
                }*/
            if (dec)
            {
                for(int i=0;i<records.Count;i++)
                    for(int j = i + 1; j < records.Count; j++)
                        if (records[i].Year() < records[j].Year())
                        {
                            var tmp = records[i];
                            records[i] = records[j];
                            records[j] = tmp;
                        }
            }
            else
            {
                for (int i = 0; i < records.Count; i++)
                    for (int j = i + 1; j < records.Count; j++)
                        if (records[i].Year() > records[j].Year())
                        {
                            var tmp = records[i];
                            records[i] = records[j];
                            records[j] = tmp;
                        }
            }
        }



    }
}
