﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LibiryWeb
{
    //Класс - описание Патента.
    public class Patent : Record
    {
        //public string name;//Название
        //public List<string> autors;//Изобретатели
        public string country;//Страна
        //public int ID;//Регистрационный номер
        public DateTime date;//Дата подачи заявки
        public DateTime datePublication;//Дата публикации
        public int countPage;//Кол-во страниц
        //public string description;//Примечание


        //Разные конструкторы
        public Patent()
        {
            name = "Что-то очень полезное и секретное";
            autors = new List<string>(1) { "Алексей Оленчиков" };
            country = "Россия";
            ID = 0;
            date = new DateTime(2019, 01, 01);
            datePublication = new DateTime(2019, 02, 02);
            countPage = 21;
            description = "";
        }
        public Patent(string name, List<string> autors, string country,  int ID,  DateTime date,  DateTime datePublication, int countPage, string description)
        {
            this.name = name;
            this.autors = autors;
            this.country = country;
            this.ID = ID;
            this.date = date;
            this.datePublication = datePublication;
            this.countPage = countPage;
            this.description = description;
        }

        //Метод о том, как выводить правильно патент на экран
        public override void Write()
        {
            Console.WriteLine("Название = " + name);
            Console.Write("Изобретатели: ");
            for (int i = 0; i < autors.Count; i++)
            {
                if (i == 0) Console.Write(autors[i]);
                else Console.Write(" ; " + autors[i]);
            }
            Console.WriteLine();
            Console.WriteLine("Страна = " + country);
            Console.WriteLine("Регистрационный номер = " + ID);
            Console.WriteLine("Дата подачи заявки = " + date);
            Console.WriteLine("Дата публикации = " + datePublication);
            Console.WriteLine("Кол-во страниц = " + countPage);
            Console.WriteLine("Примечание = " + description);
        }

        //Год
        public override int Year()
        {
            return datePublication.Year;
        }
    }
}
