﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LibiryWeb
{
    public abstract class Record
    {
        //public string type;//Тип записи
        public string name;//Название
        public string description;//Примечание
        public int ID;//Идентификационный номер
        public List<string> autors;//Авторы / изобретатели
        public string namePublish;//Название издания

        public virtual void Write() { }
        public virtual int Year() { return 0; }
    }
}
