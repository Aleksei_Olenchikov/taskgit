﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LibiryWeb;
using System.Collections.Generic;

namespace LibiryTest
{
    [TestClass]
    public class KatalogTest
    {
        [TestMethod]
        public void TestAddRecord()
        {
            var k = new Katalog();
            Book b = new Book();
            k.AddRecord(b);

            Assert.AreEqual(k.GetRecords()[k.GetRecords().Count - 1], b);

            b = new Book("1", new List<string>(1) { "1" }, "1", "1", 1, 1, "1", 1);
            k.AddRecord(b);

            Assert.AreEqual(k.GetRecords()[k.GetRecords().Count - 1], b);

            Paper p = new Paper();
            k.AddRecord(p);
            Assert.AreEqual(k.GetRecords()[k.GetRecords().Count - 1], p);

            p = new Paper("2", "2", "2", 2, 2, "2", 2, new DateTime(2, 2, 2), 2);
            k.AddRecord(p);

            Assert.AreEqual(k.GetRecords()[k.GetRecords().Count - 1], p);

            Patent pa = new Patent();
            k.AddRecord(pa);
            Assert.AreEqual(k.GetRecords()[k.GetRecords().Count - 1], pa);

            pa = new Patent("3", new List<string>(1) { "3" }, "3", 3, new DateTime(3, 3, 3), new DateTime(3, 3, 3), 3, "3");
            k.AddRecord(pa);

            Assert.AreEqual(k.GetRecords()[k.GetRecords().Count - 1], pa);

            Assert.AreNotEqual(k.GetRecords()[k.GetRecords().Count - 3], pa);


        }


        
        [TestMethod]
        public void TestDelRecords()
        {
            var k = new Katalog();
            Book b = new Book();
            k.AddRecord(b);

            Book b1 = new Book("1", new List<string>(1) { "1" }, "1", "1", 1, 1, "1", 1);
            k.AddRecord(b1);
            k.DelRecord(1);

            Assert.AreEqual(k.GetRecords()[k.GetRecords().Count - 1], b);
        }

        [TestMethod]
        public void TestFindByName()
        {
            Katalog k = new Katalog();
            Book b = new Book();

            k.AddRecord(b);
            Book b1 = new Book("1", new List<string>(1) { "1" }, "1", "1", 1, 1, "1", 1);
            k.AddRecord(b1);
            Book b2 = new Book("2", new List<string>(1) { "1" }, "1", "1", 1, 1, "1", 1);
            k.AddRecord(b2);
            Book b3 = new Book("3", new List<string>(1) { "1" }, "1", "1", 1, 1, "1", 1);
            k.AddRecord(b3);
            Book b4 = new Book("4", new List<string>(1) { "1" }, "1", "1", 1, 1, "1", 1);
            k.AddRecord(b4);
            Book b5 = new Book("5", new List<string>(1) { "1" }, "1", "1", 1, 1, "1", 1);
            k.AddRecord(b5);

            Katalog resFind = new Katalog();
            resFind = k.FindByName("1");
            Assert.AreEqual(resFind.GetRecords()[0], b1);

            resFind = k.FindByName("3");
            Assert.AreEqual(resFind.GetRecords()[0], b3);

            resFind = k.FindByName("1");
            Assert.AreEqual(resFind.GetRecords().Count, 1);

        }


        [TestMethod]
        public void TestFindBookByAutor()
        {
            Katalog k = new Katalog();
            Book b1 = new Book("1", new List<string>(1) { "1" }, "1", "1", 1, 1, "1", 1);
            k.AddRecord(b1);
            Book b2 = new Book("2", new List<string>(2) { "1","2" }, "1", "1", 1, 1, "1", 1);
            k.AddRecord(b2);
            Book b3 = new Book("3", new List<string>(1) { "1" }, "1", "1", 1, 1, "1", 1);
            k.AddRecord(b3);
            Book b4 = new Book("4", new List<string>(2) { "2","3" }, "1", "1", 1, 1, "1", 1);
            k.AddRecord(b4);
            Book b5 = new Book("5", new List<string>(1) { "1" }, "1", "1", 1, 1, "1", 1);
            k.AddRecord(b5);

            Katalog resFind = new Katalog();
            resFind = k.FindBookByAutor("1");
            Assert.AreEqual(resFind.GetRecords().Count, 4);

            resFind = k.FindBookByAutor("2");
            Assert.AreEqual(resFind.GetRecords().Count, 2);

            resFind = k.FindBookByAutor("3");
            Assert.AreEqual(resFind.GetRecords().Count, 1);

            resFind = k.FindBookByAutor("4");
            Assert.AreEqual(resFind.GetRecords().Count, 0);
        }

        [TestMethod]
        public void TestSortRecordByYear()
        {
            Katalog k = new Katalog();
            Book b1 = new Book("1", new List<string>(1) { "1" }, "1", "1", 1, 1, "1", 1);
            k.AddRecord(b1);
            Book b2 = new Book("2", new List<string>(2) { "1", "2" }, "1", "1", 4, 4, "1", 1);
            k.AddRecord(b2);
            Book b3 = new Book("3", new List<string>(1) { "1" }, "1", "1", 3, 3, "1", 1);
            k.AddRecord(b3);
            Book b4 = new Book("4", new List<string>(2) { "2", "3" }, "1", "1", 2, 2, "1", 1);
            k.AddRecord(b4);
            Book b5 = new Book("5", new List<string>(1) { "1" }, "1", "1", 5, 5, "1", 1);
            k.AddRecord(b5);

            k.SortRecordsByYear(false);

            Assert.AreEqual(k.GetRecords()[0], b1);
            Assert.AreEqual(k.GetRecords()[1], b4);
            Assert.AreEqual(k.GetRecords()[4], b5);

            k.SortRecordsByYear(true);

            Assert.AreEqual(k.GetRecords()[4], b1);
            Assert.AreEqual(k.GetRecords()[3], b4);
            Assert.AreEqual(k.GetRecords()[2], b3);
        }


    }
}
