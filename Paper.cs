﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LibiryWeb
{
    //Класс - описание Газеты
    public class Paper : Record
    {
        //public string name;//Название
        public string city;//город
        //public string namePublish;//Название издательства
        public int year;//Год издания
        public int countPage;//Кол-во страниц
        //public string description;//Примечание
        public int number;//Номер
        public DateTime date;//Дата
        //public int ID;//Идентификационный номер серийного издания

        //Разные конструкторы
        public Paper()
        {
            //type = "Paper";
            name ="Мурзилка";
            city="Ижевск";
            namePublish="Удмуртский вестник";
            year=2019;
            countPage=21;
            description="";
            number=0;
            date=new DateTime(2019,09,25);
            ID=0;
        }
        public Paper(string name, string city, string namePublish, int year, int countPage, string description, int number, DateTime date, int ID)
        {
            //type = "Paper";
            this.name = name;
            this.city = city;
            this.namePublish = namePublish;
            this.year = year;
            this.countPage = countPage;
            this.description = description;
            this.number = number;
            this.date = date;
            this.ID = ID;
        }

        //Метод о том, как выводить правильно газету на экран
        public override void Write()
        {
            Console.WriteLine("Название = " + name);
            Console.WriteLine("Город = " + city);
            Console.WriteLine("Название издательства = " + namePublish);
            Console.WriteLine("Год издания = " + year);
            Console.WriteLine("Кол-во страниц = " + countPage);
            Console.WriteLine("Примечание = " + description);
            Console.WriteLine("Номер = " + number);
            Console.WriteLine("Дата = " + date.ToString());
            Console.WriteLine("Идентификационный номер серийного издания = " + ID);
        }

        //Год
        public override int Year()
        {
            return year;
        }
    }
}
