﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LibiryWeb
{
    //Класс - описание книги.
    public class Book : Record
    {
        //public string name;//Название
        //public List<string> autors;//Авторы
        public string city;//город
        //public string namePublish;//Название издательства
        public int year;//Год издания
        public int countPage;//Кол-во страниц
        //public string description;//Примечание
        //public int ID;//Идентификационный номер книги


        //Всякие конструкторы класса
        public Book()
        {
            //type = "Book";
            name = "Autobiography";
            autors = new List<string>(1) { "Alexey" };
            city = "Izhevsk";
            namePublish = "Удмуртский вестник";
            year = 2019;
            countPage = 21;
            description = "";
            ID = 0;
        }
        public Book(string name, List<string> autors, string city, string namePublish, int year, int countPage, string description, int ID)
        {
            //type = "Book";
            this.name = name;
            this.autors = autors;
            this.city = city;
            this.namePublish = namePublish;
            this.year = year;
            this.countPage = countPage;
            this.description = description;
            this.ID = ID;
        }

        //Метод о том, как выводить правильно книгу на экран
        public override void Write()
        {
            Console.WriteLine("Название = " + name);
            Console.Write("Авторы: ");
            for(int i = 0; i < autors.Count; i++)
            {
                if (i == 0) Console.Write(autors[i]);
                else Console.Write(" ; " + autors[i]);
            }
            Console.WriteLine();
            Console.WriteLine("Город = " + city);
            Console.WriteLine("Название издательства = " + namePublish);
            Console.WriteLine("Год издания = " + year);
            Console.WriteLine("Кол-во страниц = " + countPage);
            Console.WriteLine("Примечание = " + description);
            Console.WriteLine("Идентификационный номер книги = " + ID);
        }

        //Год
        public override int Year()
        {
            return year;
        }
    }
}
